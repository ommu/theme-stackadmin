<?php 
/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
?>

<?php if($modalHeader) {?>
    <div class="modal-header">
        <h5 class="modal-title"><?php echo Html::encode($this->title) ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
    </div>
<?php }?>

<div class="modal-body">
    <?php echo $content; ?>
</div>