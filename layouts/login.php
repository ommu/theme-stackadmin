<?php
/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use yii\helpers\Url;
use themes\stackadmin\assets\ThemeAsset;

$themeAsset = ThemeAsset::register($this);
\themes\stackadmin\assets\BootstrapPluginAsset::register($this);
\themes\stackadmin\assets\MaterialDesignKitFactoryPluginAsset::register($this);
\themes\stackadmin\assets\ThemeCustomAsset::register($this);
$dir = Yii::$app->request->get('dir') ? Yii::$app->request->get('dir') : 'ltr';

$isDemoTheme = Yii::$app->isDemoTheme() ? true : false;
$isLoginLayout = property_exists($this->context, 'isLoginLayout') && $this->context->isLoginLayout ? true : false;
$loginSignupWithSocialMedia = isset(Yii::$app->params['user']['loginSignupWithSocialMedia']) && Yii::$app->params['user']['loginSignupWithSocialMedia'] ? true : false;

if(!$isDemoTheme) {
	$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'theme_loginlayout']), 'default');
	if(static::$isBackoffice)
		$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'backoffice_theme_loginlayout']), 'default');
} else {
	$loginLayout = 'default';
	if(($type = Yii::$app->request->get('type')) != null)
		$loginLayout = $type;
}

$this->beginPage();?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language ?>" dir="<?php echo $dir;?>">
<head>
	<meta charset="<?php echo Yii::$app->charset ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php echo Html::csrfMetaTags() ?>
	<title><?php echo Html::encode($this->pageTitle) ?></title>
	<?php $this->head();
	$baseUrl = Yii::getAlias('@web');
$js = <<<JS
	const baseUrl = '{$baseUrl}';
	const themeAssetUrl = '{$themeAsset->baseUrl}';
	const version = '1';
if ('serviceWorker' in navigator) {
	window.addEventListener('load', function() {
		navigator.serviceWorker.register(baseUrl + '/service-worker.js?v='+version+'&bu='+baseUrl+'&tu='+themeAssetUrl);
	});
}
JS;
$this->registerJs($js, $this::POS_HEAD); ?>
</head>

<body class="<?php echo $loginLayout == 'default' ? 'layout-login' : 'layout-login-centered-boxed';?>" <?php echo isset(Yii::$app->params['stackadmin']['bgLogin']) ? 'style="background-image: url('.Yii::getAlias(Yii::$app->params['stackadmin']['bgLogin']).');"': '';?>>
<?php $this->beginBody();?>

<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-body">
				loading...
			</div>
		</div>
	</div>
</div>

<?php if($loginLayout == 'default') {?>
	<div class="layout-login__overlay"></div>
	<div class="layout-login__form bg-white" data-simplebar>
		<div class="d-flex justify-content-center mt-2 mb-5 navbar-light">
			<a href="<?php echo Url::home();?>" class="navbar-brand" style="min-width: 0">
				<img class="navbar-brand-icon" src="<?php echo $themeAsset->baseUrl;?>/images/stack-logo-blue.svg" width="25" alt="<?php echo Yii::$app->name;?>">
				<span><?php echo Yii::$app->name;?></span>
			</a>
		</div>

		<?php if($this->titleShow) {?>
			<h4 class="mb-1"><?php echo $this->title;?></h4>
		<?php }
		if($this->descriptionShow)
			echo $this->description ? '<p>'.$this->description.'</p>' : ''; ?>
		<div class="mb-5"></div>

		<?php echo \app\components\widgets\Alert::widget(['closeButton'=>false, 'options'=>['soft'=>true, 'template'=>'<i class="material-icons mr-3">check_circle</i><div class="text-body">{message}</div>', 'class'=>'mb-5']]); ?>

		<?php if($isLoginLayout && $loginSignupWithSocialMedia) {?>
		<a href="#" class="btn btn-light btn-block">
			<span class="fab fa-google mr-2"></span>
			Continue with Google
		</a>

		<div class="page-separator">
			<div class="page-separator__text">or</div>
		</div>
		<?php }
		
		echo $content; ?>
	</div>

<?php } else {?>
	<div class="layout-login-centered-boxed__form card">
		<div class="d-flex flex-column justify-content-center align-items-center mt-2 mb-5 navbar-light">
			<a href="<?php echo Url::home();?>" class="navbar-brand flex-column mb-2 align-items-center mr-0" style="min-width: 0">
				<img class="navbar-brand-icon mr-0 mb-2" src="<?php echo $themeAsset->baseUrl;?>/images/stack-logo-blue.svg" width="25" alt="<?php echo Yii::$app->name;?>">
				<span><?php echo Yii::$app->name;?></span>
			</a>
			<?php echo $this->description ? '<p class="m-0">'.$this->description.'</p>' : '';?>
		</div>

		<?php echo \app\components\widgets\Alert::widget(['closeButton'=>false, 'options'=>['soft'=>true, 'template'=>'<i class="material-icons mr-3">check_circle</i><div class="text-body">{message}</div>']]); ?>

		<?php if($isLoginLayout && $loginSignupWithSocialMedia) {?>
		<a href="#" class="btn btn-light btn-block">
			<span class="fab fa-google mr-2"></span>
			Continue with Google
		</a>

		<div class="page-separator">
			<div class="page-separator__text">or</div>
		</div>
		<?php }
		
		echo $content; ?>
	</div>
<?php }

$this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
