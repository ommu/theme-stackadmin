<?php 
/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="card">
    <?php if(isset($title)) {?>
    <div class="card-header card-header-large bg-white">
        <h4 class="card-header__title"><?php echo $title;?></h4>
    </div>
    <?php }?>
	<div class="<?php echo $paddingBody ? 'card-body' : 'list-group';?> <?php echo $textAlign ? 'text-'.$textAlign : '';?>">
		<?php echo $content; ?>
	</div>
</div>