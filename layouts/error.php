<?php 
/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;
?>

<?php /* @var $this Controller */ ?>
<?php $this->beginContent('@themes/stackadmin/layouts/login.php'); ?>

<?php echo $content; ?>

<?php $this->endContent(); ?>