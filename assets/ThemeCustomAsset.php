<?php
namespace themes\stackadmin\assets;

class ThemeCustomAsset extends \yii\web\AssetBundle
{
	public $sourcePath = '@themes/stackadmin';
	
	public $css = [
		'css/custom.css',
	];

	public $js = [
		'js/custom.js',
	];

	public $publishOptions = [
		'forceCopy' => YII_DEBUG ? true : false,
		'except' => [
			'assets/',
			'controllers/',
			'layouts/',
			'modules/',
			'site/',
			'views/',
		],
	];
}