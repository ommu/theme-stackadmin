<?php
namespace themes\stackadmin\assets;

class ThemePluginAsset extends \yii\web\AssetBundle
{
	public $sourcePath = '@themes/stackadmin';

	public $js = [
		'js/app-settings.js',
	];

	public $depends = [
		'yii\web\JqueryAsset',
		'themes\stackadmin\assets\SimplebarPluginAsset',
		'themes\stackadmin\assets\ThemeAsset',
		'themes\stackadmin\assets\PoppersPluginAsset',
		'themes\stackadmin\assets\BootstrapPluginAsset',
		'themes\stackadmin\assets\MaterialDesignKitFactoryPluginAsset',
		'themes\stackadmin\assets\ThemeCustomAsset',
	];

	public $publishOptions = [
		'forceCopy' => YII_DEBUG ? true : false,
		'except' => [
			'assets/',
			'controllers/',
			'layouts/',
			'modules/',
			'site/',
			'views/',
		],
	];
}