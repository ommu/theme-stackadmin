<?php
namespace themes\stackadmin\assets;

class ThemeAsset extends \yii\web\AssetBundle
{
	public $sourcePath = '@themes/stackadmin';
	
	public $css = [
		'css/app.css',
		// 'css/app.rtl.css',
		'css/material-icons.css',
		// 'css/material-icons.rtl.css',
		'css/fontawesome.css',
		// 'css/fontawesome.rtl.css',
		'https://fonts.googleapis.com/css?family=Open+Sans:600,700|Source+Sans+Pro:400,400i,600,700',
	];

	public $depends = [
		'themes\stackadmin\assets\GlyphiconsAsset',
	];

	public $publishOptions = [
		'forceCopy' => YII_DEBUG ? true : false,
		'except' => [
			'assets/',
			'controllers/',
			'layouts/',
			'modules/',
			'site/',
			'views/',
		],
	];
}