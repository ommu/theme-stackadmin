theme-stackadmin
=============
Stack Admin - Bootstrap 4 Dashboard Template


Installation
------------
The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist ommu/stackadmin "dev-master"
```

 or
```
 "ommu/stackadmin": "dev-master"
```

to the require section of your composer.json.


Preview
------------
https://theme.ommu.co/stackadmin-site
