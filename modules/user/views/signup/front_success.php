<?php
/**
 * @var $this app\components\View
 * @var $this app\modules\user\controllers\SignupController
 * @var $model ommu\users\models\Users
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 OMMU (www.ommu.id)
 * @created date 19 November 2018, 06:26 WIB
 * @link https://bitbucket.org/ommu/theme-stackadmin
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\ActiveForm;

$this->context->layout = 'login';

$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'theme_loginlayout']), 'default');
if(static::$isBackoffice)
	$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'backoffice_theme_loginlayout']), 'default');
?>

<div class="form-group text-center mt-4">
	<?php echo Html::a(Yii::t('app', 'Login!'), Url::to(['/site/login']), ['class' => $loginLayout == 'default' ? 'btn btn-primary' : 'btn btn-block btn-primary']); ?>
</div>