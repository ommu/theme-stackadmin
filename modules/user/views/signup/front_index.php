<?php
/**
 * @var $this app\components\View
 * @var $this app\modules\user\controllers\SignupController
 * @var $model ommu\users\models\Users
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 OMMU (www.ommu.id)
 * @created date 19 November 2018, 06:26 WIB
 * @link https://bitbucket.org/ommu/theme-stackadmin
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\ActiveForm;

$this->context->layout = 'login';

$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'theme_loginlayout']), 'default');
if(static::$isBackoffice)
	$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'backoffice_theme_loginlayout']), 'default');
?>

<?php $form = ActiveForm::begin([
	'enableClientValidation' => false,
	'enableAjaxValidation' => false,
	//'enableClientScript' => true,
]); ?>

<?php //echo $form->errorSummary($model);?>

<?php echo $form->field($model, 'displayname', ['template' => '{label}<div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-user"></i></div></div>{input}</div>{error}{hint}', 'options'=>['class'=>'form-group']])
	->textInput(['maxlength'=>true, 'placeholder' => $model->getAttributeLabel('displayname')])
	->label($model->getAttributeLabel('displayname'), ['class'=>'text-label']); ?>

<?php echo $setting->signup_username == 1 ? $form->field($model, 'username', ['template' => '{label}<div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-user-circle"></i></div></div>{input}</div>{error}{hint}', 'options'=>['class'=>'form-group']])
	->textInput(['maxlength'=>true, 'placeholder' => $model->getAttributeLabel('username')])
	->label($model->getAttributeLabel('username'), ['class'=>'text-label']) : ''; ?>

<?php echo $form->field($model, 'email', ['template' => '{label}<div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-envelope"></i></div></div>{input}</div>{error}{hint}', 'options'=>['class'=>'form-group']])
	->textInput(['maxlength'=>true, 'placeholder' => $model->getAttributeLabel('email')])
	->label($model->getAttributeLabel('email'), ['class'=>'text-label']); ?>

<?php echo Yii::$app->isSocialMedia() && $setting->signup_inviteonly != 0 && $setting->signup_checkemail == 1 ? $form->field($model, 'inviteCode', ['template' => '{label}<div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-key"></i></div></div>{input}</div>{error}{hint}', 'options'=>['class'=>'form-group']])
	->passwordInput(['placeholder' => $model->getAttributeLabel('inviteCode')])
	->label($model->getAttributeLabel('inviteCode'), ['class'=>'text-label']) : ''; ?>

<?php echo $setting->signup_random == 0 ? $form->field($model, 'password', ['template' => '{label}<div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-key"></i></div></div>{input}</div>{error}{hint}', 'options'=>['class'=>'form-group']])
	->passwordInput(['placeholder' => $model->getAttributeLabel('password')])
	->label($model->getAttributeLabel('password'), ['class'=>'text-label'])
	->hint(Yii::t('app', 'Choose a password that will be hard for others to guess.')) : ''; ?>

<div class="form-group text-center mt-5">
	<?php echo Html::submitButton(Yii::t('app', 'Create Account'), ['class' => $loginLayout == 'default' ? 'btn btn-primary' : 'btn btn-block btn-primary']); ?>
	<div class="clearfix mb-2"></div>

	<?php
	if(Yii::$app->id == 'back3nd')
		$loginUrl = Url::to(['/admin/login']);
	$loginUrl = Url::to(['/site/login']);
	echo Yii::t('app', 'Have an account? {login-link}', ['login-link' => Html::a(Yii::t('app', 'Login'), $loginUrl, ['class'=>'text-body text-underline'])]);?>
</div>

<?php ActiveForm::end(); ?>