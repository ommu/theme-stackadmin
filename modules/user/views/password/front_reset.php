<?php
/**
 * @var $this app\components\View
 * @var $this app\modules\user\controllers\PasswordController
 * @var $model ommu\users\models\UserForgot
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 OMMU (www.ommu.id)
 * @created date 27 November 2018, 09:40 WIB
 * @link https://bitbucket.org/ommu/theme-stackadmin
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\ActiveForm;

$this->context->layout = 'login';

$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'theme_loginlayout']), 'default');
if(static::$isBackoffice)
	$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'backoffice_theme_loginlayout']), 'default');
?>

<?php if(isset($render) && $render == 'valid') {
	if(!$msg) {
		$form = ActiveForm::begin([
			'enableClientValidation' => false,
			'enableAjaxValidation' => false,
			//'enableClientScript' => true,
		]); ?>
		
		<?php //echo $form->errorSummary($model);?>
		
		<?php if(!$model->isNewRecord && !$model->getErrors())
			$model->password = '';
		echo $form->field($model, 'password', ['template' => '{label}<div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-key"></i></div></div>{input}</div>{error}{hint}', 'options'=>['class'=>'form-group']])
			->passwordInput(['placeholder' => $model->getAttributeLabel('password')])
			->label($model->getAttributeLabel('password'), ['class'=>'text-label'])
			->hint(Yii::t('app', 'Choose a password that will be hard for others to guess.')); ?>
		
		<?php echo $form->field($model, 'confirmPassword', ['template' => '{label}<div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-key"></i></div></div>{input}</div>{error}', 'options'=>['class'=>'form-group']])
			->passwordInput(['placeholder' => $model->getAttributeLabel('confirmPassword')])
			->label($model->getAttributeLabel('confirmPassword'), ['class'=>'text-label']); ?>
		
		<div class="form-group text-center <?php echo $loginLayout == 'default' ? 'mt-5' : '';?>">
			<?php echo Html::submitButton(Yii::t('app', 'Changa Password!'), ['class' => $loginLayout == 'default' ? 'btn btn-primary' : 'btn btn-block btn-primary']); ?>
		</div>
		
		<?php ActiveForm::end();

	} else {
		if(Yii::$app->id == 'back3nd')
			$loginUrl = Url::to(['/admin/login']);
		$loginUrl = Url::to(['/site/login']);?>

		<div class="form-group text-center mt-4">
			<?php echo Html::a(Yii::t('app', 'Login!'), $loginUrl, ['class' => $loginLayout == 'default' ? 'btn btn-primary' : 'btn btn-block btn-primary']); ?>
		</div>
<?php }

} else {?>
<div class="form-group text-center mt-4">
	<?php echo Html::a(Yii::t('app', 'Forgot password Again?'), Url::to(['/user/password/forgot']), ['class' => $loginLayout == 'default' ? 'btn btn-primary' : 'btn btn-block btn-primary']); ?>
</div>

<?php }?>