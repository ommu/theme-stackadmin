<?php
/**
 * @var $this app\components\View
 * @var $this app\modules\user\controllers\VerifyController
 * @var $model ommu\users\models\UserVerify
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 OMMU (www.ommu.id)
 * @created date 3 December 2018, 06:19 WIB
 * @link https://bitbucket.org/ommu/theme-stackadmin
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->context->layout = 'login';

$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'theme_loginlayout']), 'default');
if(static::$isBackoffice)
	$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'backoffice_theme_loginlayout']), 'default');
?>

<div class="form-group text-center mt-4>">
	<?php if(isset($render) && $render == 'valid')
		echo Html::a(Yii::t('app', 'Home'), Url::toRoute(['/site/index']), ['class' => $loginLayout == 'default' ? 'btn btn-primary' : 'btn btn-block btn-primary']);
	else
		echo Html::a(Yii::t('app', 'Verify Email Again?'), Url::to(['/user/verify']), ['class' => $loginLayout == 'default' ? 'btn btn-primary' : 'btn btn-block btn-primary']);?>
</div>