<?php
/**
 * @var $this app\components\View
 * @var $this app\modules\user\controllers\VerifyController
 * @var $model ommu\users\models\UserVerify
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 OMMU (www.ommu.id)
 * @created date 3 December 2018, 06:19 WIB
 * @link https://bitbucket.org/ommu/theme-stackadmin
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\ActiveForm;

$this->context->layout = 'login';

$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'theme_loginlayout']), 'default');
if(static::$isBackoffice)
	$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'backoffice_theme_loginlayout']), 'default');

$isGuest = Yii::$app->user->isGuest ? true : false;
?>

<?php if(!Yii::$app->session->hasFlash('success')) {
$form = ActiveForm::begin([
	'enableClientValidation' => false,
	'enableAjaxValidation' => false,
	//'enableClientScript' => true,
]); ?>

<?php //echo $form->errorSummary($model);?>

<?php echo $form->field($model, 'email_i', ['template' => '<div class="input-group"><div class="input-group-prepend"><div class="input-group-text"><i class="fas fa-envelope"></i></div></div>{input}</div>{error}', 'options'=>['class'=>'form-group']])
	->textInput(['placeholder' => $model->getAttributeLabel('email_i')])
	->label($model->getAttributeLabel('email_i')); ?>

<div class="form-group text-center <?php echo $loginLayout == 'default' ? 'mt-5' : '';?>">
	<?php echo Html::submitButton(Yii::t('app', 'Send Instructions!'), ['class' => $loginLayout == 'default' ? 'btn btn-primary' : 'btn btn-block btn-primary']);
	
	if($isGuest) {?>
		<div class="clearfix mb-5"></div>
		<?php echo Html::a(Yii::t('app', 'Sign up!'), Url::to(['/user/signup'])); ?><br>
		<?php echo Yii::t('app', 'Have an account? {login-link}', ['login-link' => Html::a(Yii::t('app', 'Login'), Url::to(['/site/login']), ['class'=>'text-body text-underline'])]);?>
	<?php }?>
</div>

<?php ActiveForm::end();
}?>
