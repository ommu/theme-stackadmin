<?php
/**
 * @var $this DefaultController
 * @var $this app\components\View
 * @var $generator yii\gii\Generator
 * @var $id string panel ID
 * @var $form app\components\widgets\ActiveForm
 * @var $results string
 * @var $hasError bool
 * @var $files CodeFile[]
 * @var $answers array
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2017 OMMU (www.ommu.id)
 * @created date 24 September 2017, 12:38 WIB
 * @link https://bitbucket.org/ommu/theme-stackadmin
 *
 */

use yii\helpers\Html;
use app\components\widgets\ActiveForm;
use app\components\widgets\GiiActiveField as ActiveField;
use yii\gii\CodeFile;

$templates = [];
foreach ($generator->templates as $name => $path) {
    $templates[$name] = "$name ($path)";
}
?>

<div class="default-view">
	<?php $form = ActiveForm::begin([
		'options' => ['class' => 'form-horizontal form-label-left'],
		'id' => "$id-generator",
		'successCssClass' => '',
		'fieldConfig' => [
			'class' => ActiveField::className(),
		],
	]); ?>

	<?php echo $this->renderFile($generator->formView(), [
		'generator' => $generator,
		'form' => $form,
	]); ?>

	<?php echo $form->field($generator, 'template', ['template' => '{label}{beginWrapper}{input}{error}{endWrapper}{hint}'])
		->sticky()
		->dropDownList($templates)
		->label('Code Template')
		->hint('Please select which set of the templates should be used to generated the code.') ?>

	<div class="ln_solid"></div>

	<?php $generateButton = isset($files) ? Html::submitButton('Generate', ['name' => 'generate', 'class' => 'btn btn-success']) : '';
	echo $form->field($generator, 'submitButton', ['template'=>'{beginWrapper}{input}'.$generateButton.'{endWrapper}'])
		->submitButton(['button'=>Html::submitButton('Preview', ['name' => 'preview', 'class' => 'btn btn-primary'])]); ?>

	<?php if (isset($results) || isset($files)) {?>
	<div class="ln_solid"></div>
		<?php if (isset($results)) {
			echo $this->render('@yii/gii/views/default/view/results', [
				'generator' => $generator,
				'results' => $results,
				'hasError' => $hasError,
			]);
		} elseif (isset($files)) {
			echo $this->render('@yii/gii/views/default/view/files', [
				'id' => $id,
				'generator' => $generator,
				'files' => $files,
				'answers' => $answers,
			]);
		}
	}?>

	<?php ActiveForm::end(); ?>
</div>
