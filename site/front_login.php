<?php
/**
 * @var $this app\components\View
 * @var $this app\modules\admin\controllers\LoginController
 * @var $model app\modules\user\models\LoginForm
 * @var $form app\components\widgets\ActiveForm
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 OMMU (www.ommu.id)
 * @created date 3 January 2018, 14:02 WIB
 * @link https://bitbucket.org/ommu/theme-stackadmin
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\widgets\ActiveForm;

$this->context->layout = 'login';

$this->params['breadcrumbs'][] = $this->title;

$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'theme_loginlayout']), 'default');
if(static::$isBackoffice)
	$loginLayout = Yii::$app->setting->get(join('_', [Yii::$app->id, 'backoffice_theme_loginlayout']), 'default');
?>

<?php $form = ActiveForm::begin([
	'enableClientValidation' => false,
	'enableAjaxValidation' => false,
	//'enableClientScript' => true,
]); ?>

<?php //echo $form->errorSummary($model);?>

<?php echo $form->field($model, 'username', ['template'=>'{label}<div class="input-group input-group-merge">{input}<div class="input-group-prepend"><div class="input-group-text"><span class="far fa-envelope"></span></div></div></div>{error}', 'options'=>['class'=>'form-group']])
	->textInput(['autofocus'=>true, 'class'=>'form-control form-control-prepended', 'placeholder'=>'john@doe.com'])
	->label($model->getAttributeLabel('username'), ['class'=>'text-label']); ?>

<?php echo $form->field($model, 'password', ['template'=>'{label}<div class="input-group input-group-merge">{input}<div class="input-group-prepend"><div class="input-group-text"><span class="fa fa-key"></span></div></div></div>{error}', 'options'=>['class'=>'form-group']])
	->passwordInput(['type'=>'password', 'class'=>'form-control form-control-prepended', 'placeholder'=>'Enter your password'])
	->label($model->getAttributeLabel('password'), ['class'=>'text-label']); ?>

<?php if($loginLayout != 'default') {?>
<div class="form-group">
	<?php echo Html::submitButton(Yii::t('app', 'Login'), ['class'=>'btn btn-block btn-primary']); ?>
</div>
<?php }?>

<?php echo Yii::$app->params['user']['rememberMe'] ? 
$form->field($model, 'rememberMe', ['options'=>['class'=>$loginLayout == 'default' ? 'form-group' : 'form-group text-center']])
	->checkbox(['template'=>'<div class="custom-control custom-checkbox">{input}{label}</div>', 'class'=>'custom-control-input'])
	->label(Yii::t('app', 'Remember me'), ['class'=>'custom-control-label']) : 
''; ?>

<div class="form-group text-center <?php echo $loginLayout == 'default' ? 'pt-4' : 'mt-5';?>">
	<?php if($loginLayout == 'default') {
		echo Html::submitButton(Yii::t('app', 'Login'), ['class'=>'btn btn-primary']); ?>
		<div class="clearfix mb-5"></div>
	<?php }?>
	<?php echo Html::a(Yii::t('app', 'Forgot password?'), Url::to(['/user/password/forgot'])); ?><br>
	<?php echo Yii::t('app', 'Don\'t have an account? {signup-link}', ['signup-link' => Html::a(Yii::t('app', 'Sign up!'), Url::to(['/user/signup']), ['class'=>'text-body text-underline'])]);?>
</div>

<?php ActiveForm::end(); ?>