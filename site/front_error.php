<?php
/**
 * @var $this app\components\View
 * @var $name string
 * @var $message string
 * @var $exception Exception
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2018 OMMU (www.ommu.id)
 * @created date 3 January 2018, 16:02 WIB
 * @link https://bitbucket.org/ommu/theme-stackadmin
 *
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $name;
$this->titleShow = false;

$textColor = $exception->statusCode === 404 ? "text-yellow" : "text-red";
$url = Yii::$app->request->absoluteUrl;
$message = $name.' '.nl2br(Html::encode($message));
?>

<div class="form-group text-center pt-3">
	<h1 class="error-number <?php echo $textColor;?>"><?php echo $exception->statusCode; ?></h1>
	<h3><?php echo nl2br(Html::encode($exception->getName())) ?></h3>
	<p><?php echo nl2br(Html::encode($message)); ?></p>
</div>

<div class="form-group text-center mt-5">
	<?php echo Html::a(Yii::t('app', 'Report this?'), ['/report/site/add', 'url'=>$url, 'message'=>$message], ['class'=>'modal-btn']);?>
</div>