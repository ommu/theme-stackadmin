<?php
/**
 * ListView for Stackadmin Themes
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 18 September 2019, 09:10 WIB
 * @link https://bitbucket.org/ommu/theme-stackadmin
 */

namespace themes\stackadmin\components\widgets;

use Yii;
use yii\helpers\Html;

class ListView extends \yii\widgets\ListView
{
	/**
	 * {@inheritdoc}
	 */
	public $pager = [
        'class' => 'themes\stackadmin\components\widgets\LinkPager',
	];

	/**
	 * {@inheritdoc}
	 */
	public function renderPager()
	{
		if($this->view->pagination != 'default')
			return Html::tag('div', parent::renderPager(), ['class'=>'pagination-'.$this->view->pagination]);

		return parent::renderPager();
	}
}
