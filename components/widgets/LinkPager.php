<?php
/**
 * LinkPager
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2020 OMMU (www.ommu.id)
 * @created date 5 March 2020, 18:49 WIB
 * @link https://bitbucket.org/ommu/theme-stackadmin
 */

namespace themes\stackadmin\components\widgets;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class LinkPager extends \yii\widgets\LinkPager
{
    /**
     * {@inheritdoc}
     */
    public $linkOptions = ['class' => 'page-link'];
    /**
     * {@inheritdoc}
     */
    public $pageCssClass = 'page-item';
    /**
     * {@inheritdoc}
     */
    public $firstPageCssClass = 'first page-item';
    /**
     * {@inheritdoc}
     */
    public $lastPageCssClass = 'last page-item';
    /**
     * {@inheritdoc}
     */
    public $prevPageCssClass = 'prev page-item';
    /**
     * {@inheritdoc}
     */
    public $nextPageCssClass = 'next page-item';
    /**
     * {@inheritdoc}
     */
    public $disabledListItemSubTagOptions = [
        'tag' => 'a',
        'class' => 'page-link',
    ];
    /**
     * {@inheritdoc}
     */
    public $nextPageLabel = '<span class="material-icons">chevron_right</span>';
    /**
     * {@inheritdoc}
     */
    public $prevPageLabel = '<span class="material-icons">chevron_left</span>';
    /**
     * {@inheritdoc}
     */
    public $firstPageLabel = '<span class="material-icons">first_page</span>';
    /**
     * {@inheritdoc}
     */
    public $lastPageLabel = '<span class="material-icons">last_page</span>';

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        if ($this->registerLinkTags) {
            $this->registerLinkTags();
        }

        echo Html::tag('div', $this->renderPageButtons(), ['class'=>'d-inline-block pagination-'.$this->view->pagination]);
    }
}
