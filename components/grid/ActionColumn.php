<?php
/**
 * ActionColumn for Stackadmin Themes
 *
 * @author Putra Sudaryanto <putra@ommu.id>
 * @contact (+62)856-299-4114
 * @copyright Copyright (c) 2019 OMMU (www.ommu.id)
 * @created date 6 May 2019, 04:25 WIB
 * @link https://bitbucket.org/ommu/theme-stackadmin
 */

namespace themes\stackadmin\components\grid;

use Yii;

class ActionColumn extends \yii\grid\ActionColumn
{
	
}
