<?php
namespace themes\stackadmin\components;

class Heading extends \yii\base\Widget
{
	public function run() {
		return $this->render('heading');
	}
}